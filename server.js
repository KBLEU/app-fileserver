const express = require("express");
const app = express();
const fs = require("fs");
const path = require("path");
const cors = require("cors");

const FILE1_PATH = path.resolve(__dirname, "files/black-key-icon.png");
const FILE2_PATH = path.resolve(__dirname, "files/black-key-icon.png");
const DEFAULT_PATH = path.resolve(__dirname, "files/default.txt");
const ZIP_PATH = path.resolve(__dirname, "files/022.zip");

const middleware = (req, res, next) => {
    console.log("token ", req.query.token)
    if (req.query.token == "valid") {
        next()
    } else {
        res.send("token is not valid");
    }
}

app.use(cors('*'));
app.get("/get-my-file/:img_name", middleware, (req, res) => {
    const img_name = req.params.img_name;
    if (img_name == 1 || img_name == 2) {
        const stream = fs.createReadStream(img_name == 1 ? FILE1_PATH : FILE2_PATH);
        res.sendFile(img_name == 1 ? FILE1_PATH : FILE2_PATH);
    } else {
        const stream = fs.createReadStream(DEFAULT_PATH);
        stream.pipe(res);
    }
});

app.get("/downloadzip", (req, res) => {
    const filestream = fs.createReadStream(ZIP_PATH);
    filestream.pipe(res);
});

app.get("/", (req, res) => {
    res.send("app is running...:)")
})

app.listen(3010, () => {
    console.log("app is running...")
})